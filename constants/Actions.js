/**
 * Actions to be performed on / from server
 */

/**
 * Informs a user to change room
 * @constant
 * @param room_id
 */
export const CHANGE_ROOM = "CHANGE_ROOM"

/**
 * Complete game
 * @constant
 * @param data
 */
export const COMPLETE_GAME = "COMPLETE_GAME"

/**
 * Informs the server to create a lobby
 * @constant
 * @param name
 */
export const CREATE_LOBBY = "CREATE_LOBBY"

/**
 * Sends a player update
 * @constant
 * @param accel
 * @param lx
 * @param ly
 */
export const PLAYER_UPDATE = "PLAYER_UPDATE"

/**
 * Used to send messages
 * @constant
 * @param text
 */
export const SEND_MESSAGE = "SEND_MESSAGE"

/**
 * Starts the game
 * @constant
 */
export const START_GAME = "START_GAME"

/**
 * Toggles the ready state for a player in a lobby
 * @constant
 */
export const TOGGLE_READY = "TOGGLE_READY"
