// Collision tags
const tags = []
for (let i = 0; i < 20; i++) {
	tags.push(Math.pow(2, i))
}
export const COLLISION_TAGS = tags
export const GROUND_TAG = Math.pow(2, 20)
