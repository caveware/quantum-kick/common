/**
 * List of all powerup types
 */
export default {
	BOOST: {
		id: "POWERUP_BOOST",
		name: "Boost",
	},
}
