/**
 * Details for the servers
 */
export const DEVELOPMENT = "ws://149.167.29.12:3000"
export const DEVELOPMENT_PORT = 3000
export const LOCAL = "ws://localhost:3000"
export const LOCAL_PORT = 3000
export const PRODUCTION = "wss://quantumkick.online:443"
export const PRODUCTION_PORT = 443
export const STAGING = "ws://testing.quantumkick.online:3000"
export const STAGING_PORT = 443
