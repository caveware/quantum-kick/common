/**
 * Rooms on the server
 */
export const IN_GAME = "IN_GAME"
export const LOBBY_LIST = "LOBBY_LIST"
export const LOBBY_SUFFIX = "LOBBY_"
