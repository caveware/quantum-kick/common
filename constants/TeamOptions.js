/**
 * Team options
 */
export default [{
	id: "GAME_1v1",
	name: "1 vs. 1",
	players: 2,
}, {
	id: "GAME_2v2",
	name: "2 vs. 2",
	players: 4,
}, {
	id: "GAME_3v3",
	name: "3 vs. 3",
	players: 6,
}]
