export default [{
	description: "Drops a block of anti-energy to drain the field",
	id: "ULTIMATE_ANTI_ENERGY",
	name: "Anti-Energy",
}, {
	description: "Emits a large burst of energy onto the field",
	id: "ULTIMATE_OVERCHARGE",
	name: "Overcharge",
}, {
	description: "Unleashes a burst of energy to blast enemies away",
	id: "ULTIMATE_PULSE",
	name: "Pulse",
}]
