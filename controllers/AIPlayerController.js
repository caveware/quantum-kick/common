import PlayerController from "./PlayerController.js"

/**
 * AI Player Controller
 * @module AIPlayerController
 */
export default class AIPlayerController extends PlayerController {
	/**
	 * Returns full-bore acceleration
	 * @returns {Number} Acceleration
	 */
	get acceleration () {
		return !!this.target
	}

	get boosting () {
		return 0
	}

	/**
	 *
	 * @returns {Victor} Facing vector
	 */
	get facing () {
		if (this.target) {
			const { x, z } = this.target
			return new Victor(x - this.x, z - this.z)
		}
		return new Victor(1, 0)
	}

	/**
	 * Determines next position to move to on grid.
	 * @param {Object} grid Grid to move on
	 */
	getNextBlock (grid) {
		const overview = grid.overview
		const self = grid.convertPositionToGridPoint(this.x, this.z)
		const here = new Victor(Math.floor(self.x / 3), Math.floor(self.z / 3))
		const target = {
			score: -Infinity,
			x: 0,
			y: 0,
		}

		overview.forEach((column, x) => {
			column.forEach((point, y) => {
				if (x === here.x && y === here.y) return

				const phase = this.alignment === "BLUE" ? point : -point
				const dist = here.distance(new Victor(x, y))

				const score = phase - Math.random() * 3 + dist * 2

				if (score > target.score) {
					target.x = x
					target.y = y
					target.score = score
				}
			})
		})

		this.target = grid.getTileAtIndex(target.x * 3 + 1, target.y * 3 + 1)
	}

	update (dt, grid) {
		super.update(dt, grid)

		if (this.target) {
			const target = grid.convertPositionToGridPoint(this.target.x, this.target.z)
			const self = grid.convertPositionToGridPoint(this.x, this.z)

			if (Math.abs(target.x - self.x) < 2 && Math.abs(target.z - self.z) < 2) {
				this.target = null
			}
		}

		if (!this.target) {
			this.getNextBlock(grid)
		}

		if (Math.random() < .005) {
			this.activatePowerup()
		}

		if (Math.random() < 1 / 20) {
			this.dropEnergy()
		}
	}
}