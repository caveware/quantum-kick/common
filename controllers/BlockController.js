// Classes
import ObjectController from "./ObjectController.js"

/**
 * Block Controller
 * @module BlockController
 */
export default class BlockController extends ObjectController {
	/**
	 * @constructor
	 * @param {BABYLON.Vector3} position Position of block
	 * @param {String} alignment Who the block belongs to
	 */
	constructor (position, alignment) {
		super(position)
		this._alignment = alignment || null
		this._phase = 0
		this._timer = 1
	}

	/**
	 * Which team the block belongs to
	 * @returns {String|null} Alignment of block
	 */
	get alignment () {
		return this._alignment
	}
	/**
	 * Representation of the block
	 */
	get data () {
		return {
			alignment: this.alignment,
			phase: this._phase,
			position: this._position,
			timer: this._timer,
		}
	}

	/**
	 * Phase of block
	 */
	get phase () {
		return this._phase
	}

	/**
	 * Alignment of the block
	 * @param {alignment} alignment Alignment of block
	 */
	set alignment (alignment) {
		this._alignment = alignment
	}

	/**
	 * Set phase
	 * @param {Number} phase Phase of block
	 */
	set phase (phase) {
		this._phase = phase
		// this._phase = Math.max(0, Math.min(phase, 1))
		// if (this._phase === 0) {
		// 	this.alignment = "RED"
		// } else if (this._phase === 1) {
		// 	this.alignment = "BLUE"
		// } else {
		// 	this.alignment = null
		// }
	}

	get timer () {
		return this._timer
	}

	set timer (timer) {
		this._timer = timer
	}
}
