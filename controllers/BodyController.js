import { Bodies } from "matter-js"

/**
 * @class BodyController
 */
export default class BodyController {
	/**
	 * @constructor
	 * @param {*} type Type of body to set
	 * @param {*} args Arguments for setting up body
	 */
	constructor (type, ...args) {
		this.setBody(type, args)
	}

	/**
	 * Sets the body for the controller to use
	 * @param {*} type Type of body to set
	 * @param {*} args Arguments for setting up body
	 */
	setBody (type, args) {
		switch (type) {
			case "c":
				this.body = Bodies.circle(...args)
				break
			case "r":
				this.body = Bodies.rectangle(...args)
		}
	}
}
