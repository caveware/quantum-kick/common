// Libraries
import { SAT } from "matter-js"
import Vector3 from "vector-3"

// Classes
import AIPlayerController from "./AIPlayerController.js"
import BlockController from "./BlockController.js"
import GridController from "./GridController.js"
import HazardController from "./HazardController.js"
import LocalPlayerController from "./LocalPlayerController.js"
import MutationController from "./MutationController.js"
import PhysicsController from "./PhysicsController.js"
import PowerupController from "./PowerupController.js"
import ScoreController from "./ScoreController.js"
import TimerController from "./TimerController.js"

import AntiEnergyUltimate from "../ultimates/AntiEnergyUltimate.js"
import OverchargeUltimate from "../ultimates/OverchargeUltimate.js"
import PulseUltimate from "../ultimates/PulseUltimate.js"

// Constants
const ULTIMATES = {
	ULTIMATE_ANTI_ENERGY: AntiEnergyUltimate,
	ULTIMATE_OVERCHARGE: OverchargeUltimate,
	ULTIMATE_PULSE: PulseUltimate,
}
const DEFAULT_MATCH_LENGTH = 60

/**
 * Game Controller
 * @module GameController
 */
export default class GameController {
	/**
	 * @constructor
	 * @param {Object} opts Options for game
	 * @param {Object} input Input manager
	 */
	constructor (opts, input) {
		input = input || Input

		window.gc = this

		/**
		 * COMPULSORY RULES
		 * ----------------
		 * map         - Which map to load
		 *
		 * OPTIONAL RULES
		 * --------------
		 * time        - Length of match in seconds (default: 180)
		 */
		this.opts = opts

		// Set map
		this.map = opts.map

		// Determine controls
		this.controls = opts.players.map((player) => {
			const ctrl = player.control
			if (ctrl === "key") {
				return input.createHumanInterface("key")
			} else {
				return input.createHumanInterface("pad", +ctrl)
			}
		})

		this.counting = true

		this.events = {}

		// Create physics
		this.physics = new PhysicsController(this.map.width, this.map.height, 5)

		// Perform setup
		this.blocks = this.setupBlocks()
		this.hazards = this.setupHazards()
		this.players = this.setupPlayers()
		this.setupMutation()
		this.setupTimer(3, this.onTimeUp.bind(this))

		// Set up powerup controller
		this.powerupController = new PowerupController()
		this.powerupController.bus.$on("SPAWN_POWERUP", () => {
			const block = this.blocks[Math.floor(this.blocks.length * Math.random())]
			const x = block.x
			const z = block.z
			this.powerupController.create("POWERUP_BOOST", new BABYLON.Vector3(x, 0, z))
		})

		this.ultimates = []

		this.ult_id = 1

		window.Score = new ScoreController(this.players)
	}

	/**
	 * Disposes of the game.
	 */
	dispose () {
	}

	/**
	 * Representation of game state at time
	 * @returns {Object} Representation of game state
	 */
	get data () {
		return {
			blocks: this.blocks.map((block) => block.data),
			countdown: this.counting,
			events: this.events,
			hazards: this.hazards.map((hazard) => hazard.data),
			players: this.players.map((player) => player.data),
			powerups: this.powerupController.data,
			scores: Score.data,
			team_scores: Score.team_scores,
			time_remaining: this.timer.time_left,
			ultimates: this.ultimates.map((ult) => {
				return {
					id: ult.id,
					name: ult.name,
					owner: ult.owner,
					phase: ult.phase,
					size: ult.size,
					x: ult.position.x,
					z: ult.position.y,
				}
			}),
		}
	}

	/**
	 * Representation of scores
	 * @returns {Object} Scores
	 */
	get scores () {
		const scores = {
			BLUE: 0,
			RED: 0,
			taken: 0,
			total: this.blocks.length,
		}

		// Count the specific types of blocks and overall number of taken blocks
		this.blocks.forEach((block) => {
			const alignment = block.alignment
			if (alignment) {
				scores[alignment]++
				scores.taken++
			}
		})

		return scores
	}

	/**
	 * Find and returns map layer with given ID
	 * @param {String} id Unique identifier of layer
	 * @returns {Object} Map layer
	 */
	getMapLayerByName (id) {
		return this.map.layers.find((layer) => {
			return layer.name === id
		})
	}

	/**
	 * Performed on time up
	 */
	onTimeUp () {
		if (this.counting) {
			this.counting = false
			this.timer._time = DEFAULT_MATCH_LENGTH
		} else {
			this.opts.onTimeUp()
		}
	}

	/**
	 * Sets up a layer, given layer name and class type
	 * @param {String} id Unique identifier of layer
	 * @param {Class} class_type Type of class to create
	 * @param {Function} [filter] Function to filter objects
	 * @returns {Array} Objects
	 */
	setupLayer (id, class_type, filter = () => { return true }) {
		// Determine map layer based on ID
		const map_layer = this.getMapLayerByName(id)

		// Add all objects to array
		return map_layer.objects.filter(filter).map((object, key) => {
			// Determine coordinates
			const ClassType = class_type.prototype
				? class_type
				: class_type(key)
			const x = (object.y + object.height / 2) / 4
			const z = (object.x + object.width / 2) / 4
			return new ClassType(new Vector3(x, 0, z))
		})
	}

	/**
	 * Interpret blocks from layer
	 * @param {Object} map_layer Layer to interpret
	 * @returns {Array} All blocks
	 */
	setupBlocks (map_layer) {
		const blocks = this.setupLayer("squares", BlockController)
		this._grid = new GridController(blocks, 5)
		this._grid.bodies.forEach((body) => {
			this.physics.add(body)
		})
		return blocks
	}
	/**
	 * Interpret hazards from layer
	 * @param {Object} map_layer Layer to interpret
	 * @returns {Array} All hazards
	 */
	setupHazards (map_layer) {
		return this.setupLayer("hazards", HazardController)
	}

	/**
	 * Sets up the mutation system
	 */
	setupMutation () {
		const emitters = this.map.layers.find((layer) => {
			return layer.name === "towers"
		}).objects

		this._mutation = new MutationController(emitters, this._grid)
	}

	/**
	 * Interpret players from layer
	 * @param {Object} map_layer Layer to interpret
	 * @returns {Array} All players
	 */
	setupPlayers (map_layer) {
		// Interpret players layer
		const filter = (value, key) => {
			return key < this.opts.players.length
		}

		const players = this.setupLayer("spawns", (key) => {
			return this.opts.players[key].ai
				? AIPlayerController
				: LocalPlayerController
		}, filter)

		// Attach controls to each player
		players.forEach((player, key) => {
			player.alignment = this.opts.players[key].alignment
			player.controller = this.controls[key]
			player.current_block = this.blocks.find((block) => {
				return block.x === player.x && block.z === player.z
			})
			player.id = `player_${key}`
			player.name = this.opts.players[key].name
			player._ultimates = this.opts.players[key].ultimates
			player.y = 10

			// Add each player to the physics simulation
			this.physics.add(player.body)
		})

		return players
	}

	/**
	 * Sets up the countdown timer
	 * @param {Number} [time] Length of match
	 * @param {Function} [callback] Function to call after time is up
	 */
	setupTimer (time = DEFAULT_MATCH_LENGTH, callback = () => {}) {
		this.timer = new TimerController(time, callback)
	}

	/**
	 * Updates the game
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		this.events = {}

		// this.powerupController.update(dt)

		// Update the match timer
		this.timer.update(dt)

		this._grid.grid = this.blocks

		// Update the mutation
		this._mutation.update(dt)

		if (this.counting) return

		const ultis = { }

		// Update the players
		this.players.forEach((player) => {
			ultis[player.id] = 0

			player.update(dt, this._grid)

			const { x, z } = this._grid.convertPositionToGridPoint(player.x, player.z)
			const block = this._grid.getTileAtIndex(x, z)

			if (block) {
				player.is_over_block = true
			} else {
				player.is_over_block = false
			}

			if (player.events.smashed) {
				if (block) {
					// block.phase = .5

					const positions = [
						[-1, 0],
						[-1, -1],
						[0, -1],
						[1, -1],
						[1, 0],
						[1, 1],
						[0, 1],
						[-1, 1],
					]

					positions.forEach((pos) => {
						const b = this._grid.getTileAtIndex(x - pos[0], z - pos[1])
						// if (b) b.phase = .5
					})
				}
			}

			if (player.smashing) {
				// this.players.forEach((p) => {
				// 	if (player !== p && SAT.collides(player.body, p.body)) {
				// 		p.respawn()
				// 		player.smashing = false
				// 	}
				// })
			}

			if (player.is_lasering) {
				const block = this._grid.getTileAtPosition(player.x, player.z)
				if (block) {
					if (!block.alignment) {
						Score.giveReward(player.id)
					}
					block.phase += (player.alignment === "RED" ? -1 : 1) * dt / 2
					block.player = player.id
				}
			}

			if (player.events.drop) {
				const block = this._grid.getTileAtPosition(player.x, player.z)
				if (block) {
					if (!block.alignment) {
						Score.giveReward(player.id)
					}
					block.alignment = player.alignment
					block.phase = 4
					block.player = player.id
				}
			}

			player.events.used_ultimates.forEach((ultimate) => {
				const UltimateClass = ULTIMATES[ultimate]

				if (UltimateClass) {
					const pos = new Victor(player.x, player.z)
					this.ultimates.push(new UltimateClass(this.ult_id++, player.id, 128 / 2, pos, player.alignment))
					Score.addMultiplier(player.id)
				}
			})

			this.powerupController.powerups.forEach((powerup) => {
				const dist = powerup.distanceTo(player._position)
				if (dist < 2 && !player.held_powerup) {
					player.held_powerup = powerup.collect()
					this.powerupController.remove(powerup.id)
				}
			})
		})

		const dead_ultimates = []

		this.ultimates.forEach((ultimate, id) => {
			ultimate.update(dt, {
				grid: this._grid,
				players: this.players,
			})

			if (ultimate.events.complete) {
				dead_ultimates.push(id)
			} else {
				ultis[ultimate.owner]++
			}
		})

		this.ultimates = this.ultimates.filter((ultimate, id) => {
			return dead_ultimates.indexOf(id) === -1
		})

		// Update active abilities
		Object.keys(ultis).forEach((player) => {
			Score.setActiveAbilities(player, ultis[player])
		})

		Score.update(dt)

		// Update physics
		this.physics.update(dt)
	}
}
