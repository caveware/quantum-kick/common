// Libraries
import { Bodies } from "matter-js"

// Constants
import { GROUND_TAG } from "../constants/PhysicsConstants"
const OVERVIEW_FINENESS = 3

/**
 * Grid Controller
 * @module GridController
 * @description Builds a grid based on given blocks
 */
export default class GridController {
	/**
	 * @constructor
	 * @param {Array} blocks All blocks for grid
	 * @param {Number} grid_size Size of grid
	 */
	constructor (blocks, grid_size) {
		this._grid_size = grid_size

		// Determine minimum block position
		this._min = blocks.reduce((min, block) => {
			if (!min.x || min.x > block.x) { min.x = block.x }
			if (!min.z || min.z > block.z) { min.z = block.z }
			return min
		}, { x: null, z: null })

		// Convert blocks array into grid
		this.grid = blocks
	}

	/**
	 * Bodies
	 * @returns {Array} Bodies
	 */
	get bodies () {
		return this._bodies || []
	}

	/**
	 * This grid
	 * @returns {Array} Grid
	 */
	get grid () {
		return this._grid
	}

	/**
	 * Size of grid block
	 * @returns {Number} Size
	 */
	get grid_size () {
		return this._grid_size
	}

	/**
	 * Minimum grid position
	 * @return {Object} Minimum position
	*/
	get min () {
		return this._min
	}

	/**
	 * Overview of grid phases for AI
	 * @return {Array} Overview of phases
	 */
	get overview () {
		return this._overview
	}

	/**
	 * Sets the grid blocks
	 * @param {Array} blocks Blocks
	 */
	set grid (blocks) {
		this._overview = []

		if (this.grid) {
			this.grid.forEach((column, x) => {
				column.forEach((space, y) => {
					const ax = Math.floor(x / OVERVIEW_FINENESS)
					const ay = Math.floor(y / OVERVIEW_FINENESS)

					const phase = (space && space.alignment)
						? space.alignment === "RED" ? space.phase : -space.phase
						: 0

					this._overview[ax] = this._overview[ax] || []
					this._overview[ax][ay] = this._overview[ax][ay] === undefined
						? phase
						: this._overview[ax][ay] + phase
				})
			})
			return
		}

		const size = this.grid_size
		this._blocks = blocks
		this._bodies = []
		this._grid = []

		blocks.forEach((block) => {
			let x = Math.round((block.x - this.min.x) / size)
			let z = Math.round((block.z - this.min.z) / size)

			this._bodies.push(Bodies.rectangle(block.x, block.z, size, size, {
				collisionFilter: {
					category: GROUND_TAG,
				},
				isSensor: true,
				isStatic: true,
				label: "block",
			}))

			this._grid[x] = this._grid[x] || []
			this._grid[x][z] = block

			// Overview x and y
			const ax = Math.floor(x / OVERVIEW_FINENESS)
			const az = Math.floor(z / OVERVIEW_FINENESS)

			this._overview[ax] = this._overview[ax] || []
			this._overview[ax][az] = 0
		})
	}

	/**
	 * Determine grid point from given position
	 * @param {Number} x x position
	 * @param {Number} z z position
	 * @returns {Object} Point
	 */
	convertPositionToGridPoint (x, z) {
		return {
			x: Math.round((x - this.min.x) / this.grid_size),
			z: Math.round((z - this.min.z) / this.grid_size),
		}
	}

	/**
	 * Determine tile at index
	 * @param {Number} x x index
	 * @param {Number} z z index
	 * @returns {Object|null} Tile
	 */
	getTileAtIndex (x, z) {
		return this.grid[x] ? this.grid[x][z] : undefined
	}

	/**
	 * Determine tile at position
	 * @param {Number} tx x position
	 * @param {Number} tz z position
	 * @returns {Object|null} Tile
	 */
	getTileAtPosition (tx, tz) {
		const { x, z } = this.convertPositionToGridPoint(tx, tz)
		return this.getTileAtIndex(x, z)
	}
}
