// Classes
import PlayerController from "./PlayerController.js"

/**
 * Local Player Controller
 * @module LocalPlayerController
 */
export default class LocalPlayerController extends PlayerController {
	/**
	 * @returns {Number} Acceleration
	 */
	get acceleration () {
		const a = this.controller.get(CONTROLS.LX)
		const b = this.controller.get(CONTROLS.LY)
		return Math.sqrt(a * a + b * b) > .2
	}

	/**
	 * @returns {Boolean} Whether or not boosting
	 */
	get boosting () {
		return false
		return this._juice > 25 &&
			this.controller.pressed(CONTROLS.DRIVE) &&
			this.acceleration
	}

	/**
	 * @returns {Number} Controller
	 */
	get controller () {
		return this._controller
	}

	/**
	 * @returns {Number} Facing direction
	 */
	get facing () {
		const x = this.controller.get(CONTROLS.LX)
		const y = this.controller.get(CONTROLS.LY)
		const vec = new Victor(-x, y)
		vec.rotate(-Math.PI / 4)
		return vec
	}

	/**
	 * Sets the controller
	 * @param {Object} controller Controller
	 */
	set controller (controller) {
		this._controller = controller
	}

	shoot () {
		if (this._energy_timer === 0) {
			this._energy_timer = .25
			this.dropEnergy()
		}
	}

	/**
	 * Update the controller
	 * @param {Number} dt Delta time
	 * @param {Object} grid Grid
	 */
	update (dt, grid) {
		// if (this.controller.pressed(CONTROLS.DRIVE)) {
		// 	this.activatePowerup()
		// }

		super.update(dt, grid)

		if (true || this.controller.get(CONTROLS.LASER)) {
			this.shoot()
		}

		if (this.controller.get(CONTROLS.ULTIMATE_1)) {
			this.useUltimate(1)
		}

		if (this.controller.get(CONTROLS.ULTIMATE_2)) {
			this.useUltimate(2)
		}

		if (this.controller.get(CONTROLS.ULTIMATE_3)) {
			this.useUltimate(3)
		}
	}
}
