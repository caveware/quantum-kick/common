/**
 * @author Josef Frank
 * @class MutationController
 */
export default class MutationController {
	/**
	 * @constructor
	 * @param {Object[]} emitters Zones which will emit the mutation
	 * @param {GridController} grid Controller for the grid
	 */
	constructor (emitters, grid) {
		this._emitters = emitters.map((emitter) => {
			return {
				type: emitter.name === "red" ? "BLUE" : "RED",
				x: emitter.x / 20,
				z: emitter.y / 20,
			}
		})
		this._grid = grid
		this._timer = 1
	}

	/**
	 * Updates block and it's surrounding items
	 * @param {Number} dt Delta time
	 * @param {String} alignment Alignment of emitter
	 * @param {Number} x x position
	 * @param {Number} z z position
	 */
	_updateSurrounding (dt, block, x, z) {
		let expanded = false

		// const blocks = []
		for (let ox = -1; ox <= 1; ox++) {
			for (let oz = -1; oz <= 1; oz++) {
				if (ox === 0 || oz === 0) {
					const b = this._grid.getTileAtIndex(x + ox, z + oz)
					// if (block && block.alignment !== alignment) block.phase += flow * Math.random() / 5
					if (b && block.phase > b.phase + 1) {
						if (b.alignment !== block.alignment) {
							b.alignment = block.alignment
							b.player = block.player
							Score.giveReward(block.player)
						}

						b.phase = block.phase - 1
						b.timer = 1
						expanded = true
					}
				}
			}
		}

		if (expanded) {
			block.phase--
		}
	}

	/**
	 * Updates the mutation
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		this._grid._blocks.forEach((block) => {
			if (block.alignment && block.phase > 1) {
				block.timer -= dt * 4

				if (block.timer <= 0) {
					const x = block._position.x
					const z = block._position.z
					const point = this._grid.convertPositionToGridPoint(x, z)
					this._updateSurrounding(dt, block, point.x, point.z)
					while (block.timer <= 0) block.timer++
				}
			} else block.timer = 1
		})
	}
}
