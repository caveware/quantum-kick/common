// Libraries
import Vector3 from "vector-3"

/**
 * Object Controller
 * @module ObjectController
 * @description Controller for object
 */
export default class ObjectController {
	/**
	 * @constructor
	 * @param {Vector3} position Position of object
	 */
	constructor (position) {
		this._position = position || new Vector3(0, 0, 0)
	}

	/**
	 * Dispose the object controller
	 */
	dispose () {
		this._position = null
		delete this
	}

	/**
	 * @returns {Object} Object data
	 */
	get data () {
		return {
			position: this._position,
		}
	}

	/**
	 * @returns {Number} x position
	 */
	get x () {
		return this._position.x
	}

	/**
	 * @returns {Number} y position
	 */
	get y () {
		return this._position.y
	}

	/**
	 * @returns {Number} z position
	 */
	get z () {
		return this._position.z
	}

	/**
	 * Sets the position
	 * @param {Object} pos Position
	 */
	set position (pos) {
		this._position.set(pos.x, pos.y, pos.z)
	}

	/**
	 * Sets the x position
	 * @param {Number} x x position
	 */
	set x (x) {
		this._position.x = x
	}

	/**
	 * Sets the y position
	 * @param {Number} y y position
	 */
	set y (y) {
		this._position.y = y
	}

	/**
	 * Sets the z position
	 * @param {Number} z z position
	 */
	set z (z) {
		this._position.z = z
	}
}
