import { Bodies, Engine, Events, World } from "matter-js"

import { GROUND_TAG } from "../constants/PhysicsConstants"

/**
 * @author Josef Frank
 * @class PhysicsController
 * @desc Controller of the game's physics.
 */
export default class PhysicsController {
	/**
	 * @constructor
	 * @param {Number} width Width of map
	 * @param {Number} height Height of map
	 * @param {Number} size Size of map grid bits
	 */
	constructor (width, height, size) {
		// Create the engine
		this._engine = Engine.create()

		// Internalise the world
		this._world = this._engine.world

		// Nullify the gravity
		this._world.gravity.y = 0

		// Events.on(this._engine, "collisionStart", (e) => {
		// 	e.pairs.forEach((data) => {
		// 		const a = data.bodyA
		// 		const b = data.bodyB

		// 		data.isActive = true

		// 		if (a.isSensor || b.isSensor) {
		// 			// Check for grid to player
		// 			if (isGridPlayerCollision(a, b)) {
		// 				const block = a.label === "block" ? a : b
		// 				const player = a.player || b.player
		// 				player.addGridBlock(block.id)
		// 			}
		// 		}
		// 	})
		// })

		// Events.on(this._engine, "collisionEnd", (e) => {
		// 	e.pairs.forEach((data) => {
		// 		if (data.isSensor) {
		// 			const a = data.bodyA
		// 			const b = data.bodyB

		// 			data.isActive = true

		// 			// Check for grid to player
		// 			if (isGridPlayerCollision(a, b)) {
		// 				const block = a.label === "block" ? a : b
		// 				const player = a.player || b.player
		// 				player.removeGridBlock(block.id)
		// 			}
		// 		}
		// 	})
		// })

		// this._createBounds(width * size, height * size)
	}

	/**
	 * Creates the bounding box
	 * @param {Number} width Width of bounds
	 * @param {Number} height Height of bounds
	 * @private
	 */
	_createBounds (width, height) {
		const createRectangle = (x, y, width, height) => {
			return Bodies.rectangle(x, y, width, height, { isStatic: true })
		}

		this.addArray([
			createRectangle(-50, height / 2, 100, height),
			createRectangle(width / 2, -50, width, 100),
			createRectangle(width + 50, height / 2, 100, height),
			createRectangle(width / 2, height + 50, width, 100),
		])
	}

	/**
	 * Adds a body to the controller.
	 * @param {Matter.Body} body Physics body to add
	 */
	add (body) {
		World.add(this._world, body)
	}

	/**
	 * Adds an array of bodies to the controller.
	 * @param {Matter.Body[]} bodies List of bodies to add
	 */
	addArray (bodies) {
		bodies.forEach((body) => {
			this.add(body)
		})
	}

	/**
	 * Removes a body from the controller.
	 * @param {Matter.Body} body Physics body to remove
	 */
	remove (body) {
		World.remove(this._world, body)
	}

	/**
	 * Removes an array of bodies from the controller.
	 * @param {Matter.Body[]} bodies List of bodies to remove
	 */
	removeArray (bodies) {
		bodies.forEach((body) => this.remove.bind(this))
	}

	/**
	 * Updates the engine.
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		Engine.update(this._engine, dt * 1000)
	}
}
