// Libraries
import { Body, Vector } from "matter-js"
import { easing, keyframes, tween } from "popmotion"
import Vector3 from "vector-3"

// Classes
import BodyController from "./BodyController.js"
import ObjectController from "./ObjectController.js"

// Constants
import { COLLISION_TAGS, GROUND_TAG } from "../constants/PhysicsConstants.js"

const DEFAULT_JUICE = 25

/**
 * Player Controller
 * @module PlayerController
 */
export default class PlayerController extends ObjectController {
	/**
	 * @constructor
	 * @param {Vector3} position Position of player controller
	 * @param {Vector3} rotation Rotation of player
	 * @param {Number} block_size Size of block
	 * @param {String} name Name of player
	 * @param {String[]} ultimates Ultimates
	 */
	constructor (position, rotation = new Vector3(0, 0, 0), block_size = 5, name = "", ultimates = []) {
		super(position)
		this._angle = 0
		this._block_size = block_size
		this._juice = DEFAULT_JUICE
		this._juice_stop = 0
		this._phase = 0
		this._rotation = rotation
		this._speed = 1
		this._ultimates = []
		this._ultimate_charge = [1, 1, 1]

		this.blasted = 0

		this.original_position = {
			x: position.x,
			y: position.y,
			z: position.z,
		}

		this._physics = new BodyController("c", position.x, position.z, 4, {
			friction: 0,
			frictionAir: 0,
			label: name,
			restitution: 3,
		})
		this.body.player = this
		this.events = {
			used_ultimates: [],
		}
		this.name = name

		this.active_powerups = []
		this.held_powerup = null

		this._energy_timer = 0
	}

	/**
	 * @returns {Number} Acceleration
	 */
	get acceleration () {
		return 1
	}

	/**
	 * @returns {Number} Block size
	 */
	get block_size () {
		return this._block_size
	}

	/**
	 * @returns {Matter.Body} Body of player
	 */
	get body () {
		return this._physics.body
	}

	/**
	 * @returns {Object} Representation of the data
	 */
	get data () {
		const data = {
			alignment: this.alignment,
			events: Object.assign({}, this.events),
			juice: this._juice,
			name: this.name,
			phase: this._phase,
			position: this._position,
			powerup: null,
			rotation: this._rotation,
			ultimate_charge: this._ultimate_charge,
			ultimates: this._ultimates,
		}

		this.events = {}

		if (this.current_block) {
			data.current_block = {
				x: this.current_block.x,
				z: this.current_block.z,
			}
		}

		if (this.next_block) {
			data.next_block = {
				x: this.next_block.x,
				z: this.next_block.z,
			}
		}

		if (this.held_powerup) {
			data.powerup = this.held_powerup.type
		}

		return data
	}

	/**
	 * @returns {Victor} Way that player is facing
	 */
	get facing () {
		return new Victor(1, 0)
	}

	/**
	 * Called to activate a powerup, if the player has one.
	 */
	activatePowerup () {
		if (this.held_powerup) {
			// Account for what will happen when removing powerup
			const type = this.held_powerup.type
			this.events.used_powerup = type
			this.held_powerup.bus.$on("DISPOSED", () => {
				const index = this.active_powerups.findIndex((powerup) => {
					return powerup.type === type
				})
				this.active_powerups.splice(index, 1)
			})

			// Activate the powerup and add it to the active array
			this.held_powerup.activate()
			this.active_powerups.push(this.held_powerup)

			// Remove powerup from hand
			this.held_powerup = null
		}
	}

	/**
	 * Depletes juice reserves
	 * @param {Number} value Amount to reduce juice by
	 */
	depleteJuice (value) {
		this._juice_stop = 2
		this._juice = Math.max(0, this._juice - value)
	}

	/**
	 * Drops energy onto ground
	 */
	dropEnergy () {
		this.events.drop = true
	}

	/**
	 * Ends using laser.
	 */
	endLaser () {
		if (this.is_lasering) {
			this.is_lasering = false

			this.killTweens()
			tween({
				duration: 75,
				easing: easing.backOut,
				from: { y: this.y },
				to: { y: 10 },
			}).start((data) => {
				this.y = data.y
			})
		}
	}

	/**
	 * Get next block
	 * @param {GridController} grid Grid controller
	 * @param {Object} ldev Looking deviation
	 * @returns {Object} Tile
	 */
	getNextBlock (grid, ldev) {
		const cb = this.current_block
		const {x, z} = grid.convertPositionToGridPoint(cb.x, cb.z)
		return grid.getTileAtIndex(x + ldev.x, z + ldev.y)
	}

	/**
	 * Kills all tweens
	 */
	killTweens () {
		if (this.tween) {
			this.smashing = false
			this.tween.stop()
		}
	}

	/**
	 * Drops the player on 0 energy.
	 */
	onNoEnergy () {
		return
		this._juice_stop = 1.6

		this.killTweens()
		this.tween = keyframes({
			duration: 2000,
			easings: [easing.easeOut, easing.easeOut, easing.backOut],
			times: [0, .15, .8, 1],
			values: [this.y, 0, 0, 10],
		}).start((y) => {
			this.y = y

			if (this.y === 0 && !this.is_over_block) {
				this.killTweens()
				this.events.died = true
				this.respawn()
			} else {
				// console.log(this.y)
			}
		})
	}

	/**
	 * Respawns the player.
	 */
	respawn () {
		Body.setPosition(this.body, {
			x: this.original_position.x,
			y: this.original_position.z,
		})
		this.y = 10

		this._juice_stop = 0
		this._juice = DEFAULT_JUICE
	}

	/**
	 * Smashes the grid
	 */
	smashAttack () {
		if (this.y === 10) {
			this.killTweens()
			this.events.smashed = true
			this.smashing = true

			this.tween = keyframes({
				duration: 800,
				easings: [easing.easeOut, easing.backOut],
				times: [0, .15, 1],
				values: [this.y, 0, 10],
			}).start((y) => {
				this.y = y
			}, () => {
				this.smashing = false
			})
		}
	}

	/**
	 * Starts using laser.
	 */
	startLaser () {
		if (this.y === 10) {
			this.events.lasered = true
			this.is_lasering = true

			this.killTweens()
			tween({
				duration: 75,
				easing: easing.backOut,
				from: { y: 10 },
				to: { y: 5 },
			}).start((data) => {
				this.y = data.y
			})
		}
	}

	/**
	 * Uses a fucking ultimate
	 * @param {Number} id ID of Ultimate (1, 2)
	 */
	useUltimate (id) {
		const key = id - 1
		if (this._ultimate_charge[key] === 1) {
			this._ultimate_charge[key] = 0

			this.events.used_ultimates.push(this._ultimates[key])
		}
	}

	/**
	 * Update the player controller
	 * @param {Number} dt Delta time
	 * @param {GridController} grid Grid controller
	 */
	update (dt, grid) {
		this._energy_timer = Math.max(0, this._energy_timer - dt)

		this.body.label = this.id

		// Update collisions
		const m = COLLISION_TAGS[Math.round((this.y))]
		this.body.collisionFilter.category = m
		this.body.collisionFilter.mask = m | GROUND_TAG

		// Reset events
		this.events = {
			lasering: this.is_lasering,
			used_ultimates: [],
		}

		// Update player position
		this.x = this.body.position.x
		this.z = this.body.position.y

		// Account for boosting
		const boosting = this.boosting
		this._speed = 1 + (boosting ? 350 : 0)
		if (boosting) this.depleteJuice(25)

		if (this._juice <= 0 && !this._juice_stop) {
			this._juice = 0
			this.onNoEnergy()
		}

		// Increase juice
		this._juice_stop = Math.max(0, this._juice_stop - dt)
		if (this._juice_stop === 0) {
			if (this.is_over_block) {
				this._juice = Math.min(100, this._juice + dt * 5)
			} else {
				this._juice = Math.max(0, this._juice - dt * 20)
			}
		}

		// Update all
		this.active_powerups.forEach((powerup) => {
			powerup.update(dt, this)
		})

		// Determine angle at which controller is pointing
		const vec = this.facing
		if (vec.length() > .1) { this._angle = vec.angle() }

		const v = Math.min(1, this.y / 10)
		const drive = this.acceleration ? 12.5 : 0
		const limit = 0.000001 * this._speed

		Body.applyForce(this.body, this.body.position,
			Vector.rotate({ x: 0, y: -limit * drive }, this._angle + Math.PI / 2)
		)

		// Limit velocity
		const vx = this.body.velocity.x
		const vy = this.body.velocity.y
		const ang = Math.atan2(vy, vx)

		let max_speed = 1.4
		if (this.anti) max_speed = .7

		let speed = Math.min(Math.sqrt((vx * vx) + (vy * vy)), max_speed) * v
		if (!this.acceleration) {
			speed = Math.max(0, speed - dt * 4)
		}
		if (!this.blasted) {
			Body.setVelocity(this.body, {
				x: Math.cos(ang) * speed,
				y: Math.sin(ang) * speed,
			})
		} else {
			this.blasted = Math.max(0, this.blasted - dt * 2)
		}

		// Recharge ultimates
		this._ultimate_charge[0] = Math.min(1, this._ultimate_charge[0] + dt / 4)
		this._ultimate_charge[1] = Math.min(1, this._ultimate_charge[1] + dt / 2)
		this._ultimate_charge[2] = Math.min(1, this._ultimate_charge[2] + dt / 4)
	}
}
