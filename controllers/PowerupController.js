// Libraries
import Vue from "vue"

// Classes
import PowerupItem from "../powerups/PowerupItem.js"

/**
 * Powerup Controller
 * @module PowerupController
 * @description Controls the spawning of powerups
 */
export default class PowerupController {
	/**
	 * @constructor
	 * @param {Number} [interval] Interval between spawning powerups
	 */
	constructor (interval = 5) {
		this.bus = new Vue()
		this.interval = interval
		this.timer = interval

		this.next_id = 0
		this.powerups = []
	}

	/**
	 * @returns {Object} Representation of the data
	 */
	get data () {
		return this.powerups.map((powerup) => {
			return {
				id: powerup.id,
				position: powerup.position,
				type: powerup.type,
			}
		})
	}

	/**
	 * Create a new powerup
	 * @param {String} type Type of powerup
	 * @param {Vector3} position Position of powerup
	 */
	create (type, position) {
		this.powerups.push(new PowerupItem(this.next_id++, type, position))
	}

	/**
	 * Removes an existing powerup
	 * @param {String} id Unique identifier of powerup
	 */
	remove (id) {
		const index = this.powerups.findIndex((powerup) => {
			return powerup.id === id
		})

		if (index > -1) {
			this.powerups.splice(index, 1)
		}
	}

	/**
	 * Performed upon need to spawn a powerup
	 */
	spawnPowerup () {
		this.bus.$emit("SPAWN_POWERUP")
	}

	/**
	 * Updates the powerup controller
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		this.timer -= dt

		while (this.timer <= 0) {
			this.timer += this.interval
			this.spawnPowerup()
		}
	}
}
