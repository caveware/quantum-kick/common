/**
 * @desc Score message
 */
class ScoreMessage {
	/**
	 * @constructor
	 * @param {Number} value Value of score
	 */
	constructor (id, value) {
		this.id = id
		this.TIME_MAX = 3
		this.timer = 0
		this.value = value
	}

	/**
	 * Data
	 */
	get data () {
		return {
			id: this.id,
			progress: this.timer / this.TIME_MAX,
			value: this.value,
		}
	}

	/**
	 * Update
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		this.timer += dt
		if (this.timer >= this.TIME_MAX) {
			this.timer = this.TIME_MAX
			this.dead = true
		}
	}
}

const COMBO_LENGTH = 3

/**
 * @desc Controller of scores
 */
export default class ScoreController {
	/**
	 * @constructor
	 * @param {Object[]} players Players we will be creating
	 */
	constructor (players) {
		this.instances = players.map((player) => {
			return {
				active_abilities: 0,
				alignment: player.alignment,
				combo_base: 0,
				combo_multiplier: 0,
				combo_timeout: 0,
				id: player.id,
				score: 0,
				scores: [],
			}
		})

		this.lastScoreID = 0
	}

	get data () {
		return this.instances.map((instance) => {
			return Object.assign({
				combo: this.getCombo(instance.id),
			}, instance)
		})
	}

	get team_scores () {
		const data = {
			RED: 0,
			BLUE: 0,
		}

		this.instances.forEach((instance) => {
			data[instance.alignment] += instance.score
		})

		return data
	}

	/**
	 * Add multiplier
	 * @param {String} id ID of player
	 */
	addMultiplier (id) {
		const instance = this.getInstanceById(id)

		instance.combo_timeout = COMBO_LENGTH
		instance.combo_multiplier++
	}

	/**
	 * Adds score to player instance
	 * @param {Number} id ID of player
	 * @param {Number} value Score to add
	 */
	addScore (id, value) {
		const instance = this.getInstanceById(id)
		if (value > 1000) {
			instance.scores.push(new ScoreMessage(++this.lastScoreID, value))
		}

		if (instance.combo_timeout) {
			instance.combo_base += value
		} else {
			instance.score += value
		}
	}

	/**
	 * EC
	 * @param {String} id ID
	 * @param {Boolean} give_reward GR
	 */
	endCombo (id, give_reward) {
		const instance = this.getInstanceById(id)

		instance.combo_timeout = 0
		if (give_reward) {
			this.addScore(id, instance.combo_base * instance.combo_multiplier)
		}

		instance.combo_base = 0
		instance.combo_multiplier = 0
	}

	/**
	 * Returns the combo associated with the player, if in combo
	 * @param {String} id ID of player
	 * @returns {Object | null} Combo
	 */
	getCombo (id) {
		const instance = this.getInstanceById(id)

		if (instance.combo_timeout) {
			return {
				base: instance.combo_base,
				multiplier: instance.combo_multiplier,
				phase: instance.combo_timeout / COMBO_LENGTH,
			}
		}

		return null
	}

	/**
	 * Finds instance by id
	 * @param {Number} id ID of instance
	 * @return {Object} Instance with id
	 */
	getInstanceById (id) {
		return this.instances.find((instance) => instance.id === id)
	}

	/**
	 * Gives reward to player
	 * @param {Number} id Player's ID
	 * @param {Number} [value=1] Value
	 */
	giveReward (id, value = 1) {
		const instance = this.getInstanceById(id)

		// Multiply reward if active abilities
		if (instance.active_abilities) value *= instance.active_abilities * 10

		this.addScore(id, value)
	}

	/**
	 * Sets active ability count
	 * @param {String} id ID of player
	 * @param {Number} value Number of abilities to set
	 */
	setActiveAbilities (id, value) {
		const instance = this.getInstanceById(id)
		instance.active_abilities = value
	}

	/**
	 * Updates the scores
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		this.instances.forEach((instance) => {
			if (instance.combo_timeout) {
				instance.combo_timeout -= dt

				if (instance.combo_timeout <= 0) {
					this.endCombo(instance.id, true)
				}
			}

			const scores_to_remove = []
			instance.scores.forEach((score, index) => {
				score.update(dt)
				if (score.dead) scores_to_remove.push(index)
			})

			instance.scores = instance.scores.filter((score, index) => {
				return scores_to_remove.indexOf(index) === -1
			})
		})
	}
}
