/**
 * Timer Controller
 * @module TimerController
 */
export default class TimerController {
	/**
	 * @constructor
	 * @param {Number} time Time to start on
	 * @param {Function} callback Function to call after time is up
	 */
	constructor (time, callback) {
		this._callback = callback
		this._total_time = this._time = time
	}

	/**
	 * @returns {Number} Remaining time
	 */
	get time_left () {
		return this._time
	}

	/**
	 * @returns {Number} Time at start
	 */
	get time_total () {
		return this._total_time
	}

	/**
	 * Update the timer
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		this._time = Math.max(0, this._time - dt)
		if (this._time === 0) { this._callback() }
	}
}
