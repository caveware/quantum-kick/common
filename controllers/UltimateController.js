/**
 * @desc Controls an ultimate
 */
export default class UltimateController {
	/**
	 * Base ultimate class
	 * @param {Number} id ID of ultimate
	 * @param {String} name Name of ultimate
	 * @param {String} owner ID of owner
	 * @param {Victor} position Position of ultimate
	 */
	constructor (id, name, owner, position) {
		this.events = {}
		this.id = id
		this.name = name
		this.owner = owner
		this.position = position
	}

	/**
	 * Returns the data for use
	 */
	get data () {
		return {
			events: this.events,
			name: this.name,
			owner: this.owner,
			position: this.position.toObject(),
		}
	}

	/**
	 * Update the controller
	 * @param {Number} dt Delta time
	 */
	update (dt) {
		this.events = []
	}
}
