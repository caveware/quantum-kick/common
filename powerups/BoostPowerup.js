// Classes
import GenericPowerup from "./GenericPowerup.js"

// Constants
import TYPES from "../constants/PowerupTypes.js"

/**
 * Boost Powerup
 * @module BoostPowerup
 * @description Boost powerup modifier
 */
class BoostPowerup extends GenericPowerup {
	/**
	 * @constructor
	 */
	constructor () {
		super()

		// Set timer for 2 seconds
		this.timer = 2
	}

	/**
	 * Applies powerup to player and updates
	 * @param {Number} dt Delta time
	 * @param {Object} player Player to update
	 */
	onUpdate (dt, player) {
		// Speed up player
		player._speed++

		// Update timer
		this.timer -= dt

		// Dispose self if out of time
		if (this.timer <= 0) this.dispose()
	}
}

BoostPowerup.prototype.type = TYPES.BOOST.id

export default BoostPowerup
