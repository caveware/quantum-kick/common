// Libraries
import Vue from "vue"

/**
 * Generic Powerup
 * @module GenericPowerup
 * @description Generic powerup template
 */
export default class GenericPowerup {
	/**
	 * @constructor
	 */
	constructor () {
		this.active = false
		this.bus = new Vue()
	}

	/**
	 * Activates the powerup
	 */
	activate () {
		this.active = true
	}

	/**
	 * Disposes of the powerup
	 */
	dispose () {
		this.active = false
		this.bus.$emit("DISPOSED")
	}

	/**
	 * Updates and applies powerup to player
	 * @param {Number} dt Delta time
	 * @param {Object} player Player to apply powerup to
	 */
	update (dt, player) {
		if (this.active) {
			this.onUpdate(dt, player)
		}
	}
}
