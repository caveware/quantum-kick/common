// Classes
import BoostPowerup from "./BoostPowerup.js"
import PowerupTypes from "../constants/PowerupTypes.js"

/**
 * @module PowerupItem
 * @description Powerup item
 */
export default class PowerupItem {
	/**
	 * @constructor
	 * @param {Number} id Unique identifier
	 * @param {String} type Type of powerup
	 * @param {BABYLON.Vector3} [position] Position of powerup
	 */
	constructor (id, type, position = new BABYLON.Vector3(0, 0, 0)) {
		this.id = id
		this.position = position
		this.type = type
	}

	/**
	 * Collect the powerup
	 * @returns {Object} Powerup to give to player
	 */
	collect () {
		this.dispose()
		return new this.powerup_dict[this.type]()
	}

	/**
	 * Dispose the powerup item
	 */
	dispose () {
		delete this // nephew
	}

	/**
	 * Determine distance between this powerup and a given point
	 * @param {BABYLON.Vector3} pos Position to calculate distance to
	 * @returns {Number} Calculated distance
	 */
	distanceTo (pos = new BABYLON.Vector3(0, 0, 0)) {
		// Deconstruct object
		const { x, y, z } = this.position

		// Comparisons
		const a = Math.pow(pos.x - x, 2)
		const b = Math.pow(pos.y - y, 2)
		const c = Math.pow(pos.z - z, 2)

		// Fully calculate distance
		return Math.sqrt(a + b + c)
	}
}

/**
 * Dictionary of all powerup types
 */
PowerupItem.prototype.powerup_dict = {
	[PowerupTypes.BOOST.id]: BoostPowerup,
}
