import UltimateController from "../controllers/UltimateController"

export const NAME = "ANTI_ENERGY_ULTIMATE"

/**
 * @desc PulseUltimate
 */
export default class AntiEnergyUltimate extends UltimateController {
	/**
	 * Pulse ultimate
	 * @param {String} owner Owner of the pulse ultimate
	 * @param {Number} size Size of the pulse ring
	 */
	constructor (id, owner, size, position, alignment) {
		super(id, NAME, owner, position)

		this.alignment = alignment
		this.phase = 0
		this.tagged_players = []
		this.size = size
	}

	/**
	 * Returns the data representing the game
	 */
	get data () {
		const data = super.data()
		return Object.assign(data, {
			name: this.name,
			phase: this.phase,
			size: this.size,
		})
	}

	/**
	 * Update the controller
	 * @param {Number} dt Delta time
	 * @param {Object} data List of data
	 */
	update (dt, data) {
		super.update(dt)

		const player = data.players.find((player) => {
			return player.id === this.owner
		})

		// Update phase
		this.phase += dt / 3
		if (this.phase >= 1) {
			player.anti = false
			this.events.complete = true
			this.phase = 1
		} else {
			player.anti = true
		}

		this.position.x = player.x
		this.position.y = player.z

		const pos = data.grid.convertPositionToGridPoint(this.position.x, this.position.y)
		const radius = 3
		const uvec = new Victor(pos.x, pos.z)

		let x = Math.ceil(pos.x - radius)
		while (x < pos.x + radius) {
			let y = Math.ceil(pos.z - radius)
			while (y < pos.z + radius) {
				const row = data.grid.grid[x]

				if (row) {
					const block = row[y]

					const bvec = new Victor(x, y)
					if (block && bvec.distance(uvec) <= radius) {
						block.phase = Math.max(0, block.phase - dt * 16)
						if (block.phase === 0) {
							if (block.alignment) {
								Score.giveReward(player.id)
							}
							block.alignment = null
						}
					}
				}
				y += 1
			}
			x += 1
		}

		Tower.addShake(.6666666 * dt)
	}
}
