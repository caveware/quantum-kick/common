import UltimateController from "../controllers/UltimateController"

export const NAME = "OVERCHARGE_ULTIMATE"

/**
 * @desc PulseUltimate
 */
export default class OverchargeUltimate extends UltimateController {
	/**
	 * Pulse ultimate
	 * @param {String} owner Owner of the pulse ultimate
	 * @param {Number} size Size of the pulse ring
	 */
	constructor (id, owner, size, position, alignment) {
		super(id, NAME, owner, position)

		this.alignment = alignment
		this.phase = 0
		this.tagged_players = []
		this.size = size
	}

	/**
	 * Returns the data representing the game
	 */
	get data () {
		const data = super.data()
		return Object.assign(data, {
			name: this.name,
			phase: this.phase,
			size: this.size,
		})
	}

	/**
	 * Update the controller
	 * @param {Number} dt Delta time
	 * @param {Object} data List of data
	 */
	update (dt, data) {
		super.update(dt)

		const player = data.players.find((player) => {
			return player.id === this.owner
		})

		// Update phase
		this.phase += dt / 5
		if (this.phase >= 1) {
			this.events.complete = true
			this.phase = 1
		}

		const pos = data.grid.convertPositionToGridPoint(this.position.x, this.position.y)
		const block = data.grid.getTileAtIndex(pos.x, pos.z)

		if (block) {
			if (block.alignment !== player.alignment) {
				Score.giveReward(player.id)
			}

			block.alignment = player.alignment
			block.phase = (player.alignment === "RED" ? 10 : 10)
			block.player = player.id
		}

		Tower.addShake(.6666666 * dt)
	}
}
