import {Body, Vector} from "matter-js"

import UltimateController from "../controllers/UltimateController"

export const NAME = "PULSE_ULTIMATE"

/**
 * @desc PulseUltimate
 */
export default class PulseUltimate extends UltimateController {
	/**
	 * Pulse ultimate
	 * @param {String} owner Owner of the pulse ultimate
	 * @param {Number} size Size of the pulse ring
	 */
	constructor (id, owner, size, position, alignment) {
		super(id, NAME, owner, position)

		this.alignment = alignment
		this.phase = 0
		this.size = size
		this.tagged_players = []
	}

	/**
	 * Returns the data representing the game
	 */
	get data () {
		const data = super.data()
		return Object.assign(data, {
			name: this.name,
			phase: this.phase,
			size: this.size,
		})
	}

	/**
	 * Update the controller
	 * @param {Number} dt Delta time
	 * @param {Object} data List of data
	 */
	update (dt, data) {
		super.update(dt)

		// Update phase
		this.phase += dt
		if (this.phase >= 1) {
			this.events.complete = true
			this.phase = 1
		}

		// Make players go far
		data.players.forEach((player) => {
			if (player.id !== this.owner && this.tagged_players.indexOf(player.id) === -1) {
				const distance = this.position.distance(new Victor(player.x, player.z))
				const direction = Math.atan2(player.z - this.position.y, player.x - this.position.x)

				let force = 1
				if (this.phase > .5) {
					force = 2 - this.phase * 2
				}

				if (distance < this.phase * this.size) {
					this.tagged_players.push(player.id)
					player.blasted = 1
					Body.applyForce(player.body, player.body.position, {
						x: Math.cos(direction) / 5000 * force,
						y: Math.sin(direction) / 5000 * force,
					})
				}
			}
		})
	}
}
